from django.shortcuts import render_to_response, get_object_or_404
from blog.models import Category, Post
from django.template import RequestContext

def blog_context(request):
	categories = Category.objects.all()
	home = 'http://localhost:8000/blog/'
	return {
		'categories' : categories,
		'home' : home
		}

def index(request):
	return render_to_response('blog/index.html')

def getPosts(request, page=1):
	newer = { 'valid' : True}
	older = { 'valid' : True}
	older['path'] = int(page) + 1
	newer['path'] = int(page) - 1
	if page == 1:
		index_begin = 0
		index_end = 5
	else:
		index_begin = 5 * (int(page) - 1)
		index_end = 5 * int(page)
	
	if index_begin == 0:
		newer['valid'] = False
	posts = Post.objects.all()[index_begin:index_end]
	if not Post.objects.all()[index_end: index_end + 5]:
		older['valid'] = False
	return 	{
		'newer' : newer,
		'older' : older,
		'posts' : posts
	}
		
def feed(request):
	argDict = getPosts(request, 1)
	return render_to_response('blog/blog_foundation.html', argDict, context_instance=RequestContext(request, processors=[blog_context]))

def feed_page(request, id):
	argDict = getPosts(request, id)
	return render_to_response('blog/blog_foundation.html', argDict, context_instance=RequestContext(request, processors=[blog_context]))

def about(request):
	categories = Category.objects.all()
	return render_to_response('blog/about.html', context_instance=RequestContext(request, processors=[blog_context]))

def category(request, id):
	chosen_category = get_object_or_404(Category, pk=id)
	posts = Post.objects.filter(category=chosen_category)
#	return HttpResponse("Placeholder for category: " + id)
	return render_to_response('blog/blog_foundation.html', {'category_posts' : posts, 'category': chosen_category},context_instance=RequestContext(request, processors=[blog_context]))

def get_post(request, id):
	post = get_object_or_404(Post, pk=id)
	return render_to_response('blog/blog_foundation.html', { 'post_by_id' : post },context_instance=RequestContext(request, processors=[blog_context]))

