from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'nicholas_io.views.home', name='home'),
   # url(r'^nicholas_io/', include('nicholas_io.foo.urls')),
	url(r'^$', 'blog.views.feed'),
	url(r'^page/(?P<id>\d+)/', 'blog.views.feed_page'),
	url(r'^about/', 'blog.views.about'),
	url(r'^category/(?P<id>\d+)/', 'blog.views.category'),
	url(r'^post/(?P<id>\d+)/', 'blog.views.get_post'),
)
